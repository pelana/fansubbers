from fabric.api import local, settings, show,env, get, run, prompt, cd, lcd
env.hosts = ["depamap.com"]

PATH = "/home/zodman/fansubbers/"

def _r(cmd):
    VIRTUAL_ENV = "/home/zodman/.virtualenvs/fansubbers"
    source = '. %s/bin/activate && ' % VIRTUAL_ENV
    return source + cmd


def __remote_virtualenv(command):
    run(_r(command))

def clear_cache():
    with cd(PATH):
        run("bash clear_cache.bash")

def deploy():
    with cd(PATH):
        run("git pull ")
        __remote_virtualenv("python manage.py migrate")
        run("touch fansubs/wsgi.py")

