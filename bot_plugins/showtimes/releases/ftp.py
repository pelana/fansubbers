import ftputil
import ftputil.session
import ftplib
import os
from django.conf import settings
from subprocess import call
import envoy

class Ftp:
    ignore = ""

    def _ses(self):
        self.ses = ftputil.session.session_factory(base_class=ftplib.FTP, port=22005)

    def ftp(self, path, download_directory):
        directory_to_download = os.path.join("premux", path)
        directory_local = download_directory
        self.log("downloading")
        #cmd = "ncftpget -z -T -v -R -P21 -u %s -p %s shinsoufansub.net %s %s" %(
            #settings.FTP_USER, settings.FTP_PASSWORD, directory_local, directory_to_download
        #)
#cmd = "keychain| rsync -azv xdcc@shinsoufansub.net:/home/bot/%s %s #" % (directory_to_download, directory_local)
        cmd = "bash /home/zodman/fansubbers/bot_plugins/rsync.bash %s %s "% (directory_to_download, directory_local)

        print cmd
        #self.log(cmd)
        call(cmd, shell=True)
        #r = envoy.run(cmd)
        #self.log(r.std_out)
        #self.log(r.std_err)
        self.log("ftp download terminado")
        self.dir =  os.path.join(directory_local, path)
        return self.dir

    def _get_files(self, host, ftp_scan=False):
        if ftp_scan == True:
            files = [os.path.join(path, file)
                                 for (path, dirs, files) in host.walk(host.curdir)
                                             for file in files]
            return files
        host.download("list/list.txt", "/tmp/list.txt")
        with open("/tmp/list.txt") as f:
            files = f.readlines()
        return [i.replace("/home/bot",".") for i in files]


    def ftp_crc(self, crc32, download_directory, ftp_scan=False):
        print "connecting to ftp"
        self._ses()
        with ftputil.FTPHost("shinsoufansub.net", settings.FTP_USER, settings.FTP_PASSWORD, session_factory=self.ses()) as host:
            files = self._get_files(host, ftp_scan)
            for i, filepath in enumerate(files):
                if crc32 in filepath:
                    print "found %s" % filepath
                    lc_file = filepath.split("/")[-1]
                    local_file = os.path.join(download_directory, lc_file.strip())
                    if not  os.path.isfile(local_file):
                        print "descargando .. %s" % local_file
                        host.download(filepath, local_file)
                        print "descarga finalizada ..."
                    else:
                        print "file exist"
                    return local_file


