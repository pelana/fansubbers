import binascii
import envoy
import os
import pipes

class Torrent:
    name = "out.torrent"
    def verify_crc(self, local_file):
        self.log("verify crc")
        try:
            with open(local_file, 'rb') as f:
                crc = binascii.crc32(f.read())
                return crc & 0xFFFFFFFF
            return True
        except:
            return False


    def get_name(self):
        return self.name

    def create_torrent_pack(self, local_files, torrent_data):
        self.log("create torrent pack")
        success, files = self.search_file("mkv", self.directory)
        if not success:
            return 
        announce_url_list = torrent_data.get("announce_url")
        announce_url = " -a".join(announce_url_list)
        local_file = self.get_name()
        envoy.run("rm -rf  /tmp/pack" )
        directory_name = self.name.replace(".torrent", "")
        envoy.run("mkdir -p /tmp/pack/%s" % directory_name)
        envoy.run("cp  %s /tmp/pack/%s  -rf" % ( " ".join(local_files) , directory_name ))
        out_file = pipes.quote(os.path.join(self.dir, local_file))
        cmd = "mktorrent %s -a %s -n %s  -o %s" % ("/tmp/pack/%s" % directory_name,announce_url,local_file.replace(".torrent",""),out_file)
        self.log("creando torrent torrent")
        self.log(cmd)
        envoy.run(cmd)
        s, torrent_file = self.search_file("torrent", self.directory)
        #envoy.run("rm /tmp/pack -rf ")
        if s:
            return torrent_file
         
    def create_torrent(self, local_files, torrent_data):
        envoy.run("rm -f %s/*.torrent" %self.directory)

        if len(local_files) >1:
            return self.create_torrent_pack(local_files, torrent_data)
        if len(local_files) ==1:
            local_file = local_files.pop()
            torrent_file = local_file + ".torrent"
            self.log("creando torrent")
            announce_url_list = torrent_data.get("announce_url")
            announce_url = ",".join(announce_url_list)
            cmd = "mktorrent %s -a %s -o %s" % (local_file, announce_url, torrent_file)
            r = envoy.run(cmd)
            if r.status_code !=0:
                self.log( r.std_err)
                return None
            return [torrent_file,]

