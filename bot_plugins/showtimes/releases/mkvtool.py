import os
import pipes
import envoy

class MkvTool:

    def __getfiles(self, directory):
        l = []
        for f in os.listdir(directory):
            file_ = os.path.join(directory, f)
            l.append(file_)
        return l

    def get_ass_file(self, directory):
        files = self.__getfiles(directory)
        for i in files:
            if '.ass' in i:
                return i

    def check_fonts_exists(self, directory):
        d = os.path.join(directory, "fuentes")
        return os.path.exists(d)

    def mkvmerge(self, mkvfile, directory):
        self.file = mkvfile 
        self.directory = directory
        out_file = self.file + ".out.mkv"
        ass_file = self.get_ass_file(directory)

        self.mkvmerge_cmd_list = ["mkvmerge ",
            pipes.quote(self.file),
            "-o",
            pipes.quote(out_file),
            pipes.quote(ass_file)
            ]
        #methods
        self.mkvmerge_clean()
        cmd_list = self.mkvmerge_attachments_fonts()
        cmd = " ".join(self.mkvmerge_cmd_list + cmd_list)
        #self.log(cmd)
        self.log("executing mkvmerge")
        r = envoy.run(cmd)
        if r.status_code !=0:
            self.log(r.std_err)
            self.log( r.std_out)
            return None
        return self.format_file(out_file)

    def format_file(self, out_file):
        from_file = out_file
        to_file = from_file.replace(".out.mkv","")
        cmd = "mv %s %s" % (from_file, to_file)
        #self.log(cmd)
        self.log("limpiando mkvmerge")
        envoy.run(cmd)
        return to_file

    def mkvmerge_clean(self):
        pass
   
    def mkvmerge_attachments_fonts(self):
        fonts_dir = os.path.join(self.directory, "fuentes")
        files = self.__getfiles(fonts_dir)
        cmd_list = []
        for f in files:
            cmd_list += ['--attach-file', pipes.quote(f) ]
        return cmd_list
