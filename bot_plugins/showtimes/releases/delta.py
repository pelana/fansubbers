import envoy
import os

class Delta:
    ignore_files = []

    def get_local_files(self, directory):
        files = [os.path.join(path, file)
                 for (path, dirs, files) in os.walk(directory)
                  for file in files]
        return files

    def search_file(self,  ext, directory, contains = None):
        files = self.get_local_files(directory)
        l = []
        for file in files:
            if file in self.ignore_files:
                continue
            if ext in file.split(".")[-1]:
                if contains is not None:
                    print  (contains.lower(), file.lower(), contains.lower() in file.lower())
                    if not contains.lower() in file.lower():
                        continue 
                l.append(file)
        if len(l) == 0:
            return False, "not %s found" % ext
        return True, l

    def xdelta(self, path):
        success, sh_fields = self.search_file("sh", path)
        if not success:
            self.log(sh_fields)
            return 
        success, mkv_files_origin = self.search_file("mkv", path)
        if success:
#            self.log("ignore %s" %  mkv_files_origin)
            self.ignore_files = mkv_files_origin

        for sh_field in sh_fields:
            cmd="bash %s" % (sh_field)
            #self.log("execute: %s" % cmd)
            os.chdir(path)
            t = envoy.run(cmd)
            if t.status_code != 0:
                self.log( "return code %s" % t.status_code)
                e = t.std_err
                self.log("%s" % e)
            else:
                for i in t.std_out.split("\n"):
                    self.log(i)

        success, mkv_files = self.search_file("mkv", path)
        if not success:
            self.log("archivos no encontrados")
            self.log(mkv_files)
            return None
        mkv_files.sort()
        if not success:
            self.log(mkv_files)
            return
        else:
            self.log("archivos generados")
            for i in mkv_files:
                self.log(i)

        return mkv_files


if __name__ == "__main__":
    d = Delta()
    d.xdelta("/tmp/t/shigatsu")

