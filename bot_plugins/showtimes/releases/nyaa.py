import requests
import re
from django.conf import settings
from webscraping import xpath 
class Nyaa:
    def upload_to_nyaa(self, torrent_file, url_info, description, anime, episodio):
        self.log("upload to nyaa %s" % torrent_file)
        req = requests.Session()
        url = "http://www.nyaa.se/?page=%s" 
        login_url = url %"login"
        data_login = {'login':settings.NYAA_USER, 'password':settings.NYAA_PASSWORD, "method":1,"submit":"Submit" }
        res = req.post(login_url, data=data_login)

        if res.status_code != 200:
            return False, "status code error %s" % res.status_code

        if "Login failed" in res.text:
            return False, "login failed"

        if not "id" in res.cookies and not "pw" in res.cookies:
            return False, "login cookies not set"

        upload_url = url % "upload"

        desc = description # % dict(anime=anime, episodio=episodio)

        data = {"catid": "1_38",
                "info": url_info, "description": desc, "rules": 1,
                'submit':"Upload", "remake": "0", "anonymous": "0", "hidden": "1"}
        files = {'torrent': open(torrent_file,"rb")}
        res = req.post(upload_url, data, files = files)
        res_text = res.text
        if "Upload failed" in res_text:
            error = xpath.get(res_text, "//[@class='error']/text()")
            return False, error

        reg = re.compile('tid\=([\d].+?)\"\>View your torrent.')
        match = reg.search(res.text)
        if match:
            r = match.groups()[0]
            url =  "http://www.nyaa.se/?page=view&tid=%s" %r
            return True, url
        return True, "uploaded"


