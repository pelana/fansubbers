# encoding=utf8
from pyaib.plugins import keyword, plugin_class
from .ftp import Ftp
from .nyaa import Nyaa
from .delta import Delta
from .torrent import Torrent
from .mkvtool import MkvTool
from ..utils import is_admin_method

from subbers.models import Fansub
from airing.models import Serie as Anime
import textwrap
from django.conf import settings
import envoy
import os 
import pipes

@plugin_class
class Release(Ftp, Nyaa, Torrent, Delta, MkvTool):
    announce_url = [
        "http://open.nyaatorrents.info:6544/announce",
        "http://tracker.frozen-layer.net:6969/announce"
    ]
    download_directory = "/home/zodman/btsync/fansub/"

    def log(self, msg):
        if settings.FTP_PASSWORD in msg:
            msg = msg.replace(settings.FTP_PASSWORD, "*****")
        msgs = textwrap.wrap(msg, 510)
        for msg in msgs:
            self.msg.reply(msg)
            #self.irc.RAW("NOTICE %s :%s" % (self.msg.nick, msg))


    def __init__(self, irc_context, config):
        print("Release Plugin Loaded!")

    @keyword("rilis_cp")
    @is_admin_method
    def exec_cp(self, irc, msg, tr, args, kwargs):
        """<path> <*files*mkv> copia rilis al xdcc """
        path = args[0]
        files = args[1]
        self.msg = msg
        self.irc = irc
        directory_to_download = os.path.join(self.download_directory, path)
        cmd = "find  %s -iname '%s' | wc  -l "  %(directory_to_download, files)
        r = envoy.run(cmd)
        msg.reply("encontrados %s" % r.std_out)
        msg.reply("Listos para subir")
#        cmd = "keychain | find  %s -iname '%s' -exec rsync -zav {}  xdcc@shinsoufansub.net:/home/bot/XDCC/pending \\;"  %(directory_to_download, files)
        cmd = "bash /home/zodman/fansubbers/bot_plugins/rsync_cp.bash %s %s %s "% (directory_to_download, files)
        r = envoy.run(cmd)
        msg.reply("finalizado! ya esta en el pending ")

    @keyword("rilis_delete", "rilis_borrar")
    @keyword.autohelp
    @is_admin_method
    def exec_delete(self, irc, msg, trigger, args, kwargs):
        """ <path> <*files*> """
        path = args[0]
        files = args[1]
        self.msg = msg
        self.irc = irc

        directory_to_download = os.path.join(self.download_directory, path)
        cmd = "find  %s -iname '%s'| wc -l  "  %(directory_to_download, files)
        r = envoy.run(cmd)
        msg.reply("encontrados %s" % r.std_out)
        cmd = "find  %s -iname '%s' -delete "  %(directory_to_download, files)
        r = envoy.run(cmd)
        msg.reply("borrados!")
        if r.status_code !=0:
            self.log(r.std_err)
        self.log(r.std_out)

    @keyword("release","rilis")
    @keyword.autohelp
    @is_admin_method
    def exec_release(self, irc, msg, trigger, args, kwargs):
        """ <fansub_slug> <anime_slug> <episodio> <directorio_ftp>"""
        self.msg = msg
        self.irc = irc
        print args
        fansub_slug = args[0]
        anime_slug = args[1]
        episodio = args[2]
        crc = args[3]
        if len(args) >3:
            self.name = args[4]
        anime = Anime.objects.filter(slug__icontains=anime_slug)
        if not  anime.exists():
            msg.reply("anime no existe")
            return
        fansub = Fansub.objects.filter(slug__icontains=fansub_slug)
        if not fansub.exists():
            msg.reply("fansub no existe")
        self.fansub = fansub[0]
        url_info = '[url="%s"]%s[/url]' %(fansub[0].website, self.fansub.name)
        description = fansub[0].description_nyaa
        anime =anime[0].name
        torrent_data = {'announce_url': self.announce_url, 'url_info': url_info, 
                'anime': anime, 'episode':episodio, 'description': description,
                    }
        self.ignore_files = [] 
        files_muxed  = self.release(url_info, description, anime, episodio,self.download_directory, crc)
        if files_muxed is not None and len(files_muxed) > 0 :
            torrents_file = self.gen_torrent(files_muxed, torrent_data)

            if not torrents_file:
                self.log("Not torrent file generated")
                return 
            for torrent_file in torrents_file:
                if torrent_file:
                    success =  self.upload(torrent_file, torrent_data)
                    self.msg.reply(success)
                    self.msg.reply(success.replace("view","download"))
        msg.reply(u"Rilís terminado.")

    def release(self,url_info, description, anime, episodio, download_directory, crc):
        # descargar el directorio y devuelve el path
        downloaded_directory = self.ftp(crc, download_directory)
        self.directory = downloaded_directory
        # aplicar xdelta si es necesario
        if not downloaded_directory:
            self.log("no se descarga el directorio")
            return 
        mkv_files = self.xdelta(downloaded_directory)
        if not mkv_files:
            self.log("no se encontro el archivo mkv nuevos")
            return 

        if self.check_fonts_exists(downloaded_directory):
        # agregar fuentes y ass
            mkv_muxeds = []
            for mkv_file in mkv_files:
                self.log("genera mkv files %s" % mkv_file)
                mkv_muxed = self.mkvmerge(mkv_files, downloaded_directory)
                if mkv_muxed:
                    mkv_muxeds.append(mkv_muxed)
            return mkv_muxeds
        else:
            return mkv_files

    def gen_torrent(self, mkvs_muxed, torrent_data):
        torrent_file = self.create_torrent(mkvs_muxed, torrent_data)
        return torrent_file

    def upload(self, torrent_file, torrent_data):
        url_info = torrent_data.get("url_info")
        description = torrent_data.get("description")
        anime = torrent_data.get("anime")
        episodio = torrent_data.get("episodio")
        success, answer= self.upload_to_nyaa(torrent_file, url_info, description, anime, episodio)
        self.log(answer)
        if success:
            return "torrent link  %s" % answer
        else:
            return  answer

if __name__ == "__main__":
    import sys
    url_info = "http://hoshizorasubs.net/"
    description = u"""Este episodio %(episodio)s de %(anime)s, es publicado por [url="http://hoshizorasubs.net/"]Hoshizora[/url]
    """

    anime ="No game no life"
    episodio ="pack"

    announce_url = [
        "http://open.nyaatorrents.info:6544/announce",
        "http://tracker.frozen-layer.net:6969/announce"
    ]
    download_directory = "/home/zodman/btsync/public/fansub/"
    crc = sys.argv[1]
    torrent_data = {'announce_url': announce_url, 'url_info': url_info, 
            'anime': anime, 'episode':episodio, 'description': description,
                }
    r = Release(None, None)
    file_muxed  = r.release(url_info, description, anime, episodio,download_directory, crc)
    if file_muxed:
        torrent_file = r.gen_torrent(file_muxed, torrent_data)
        print r.upload(torrent_file, torrent_data)

