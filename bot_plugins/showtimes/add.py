from airing.models import Work, Serie
from subbers.models import Access, Fansub
from airing.models import Serie, Work
from pyaib.plugins import keyword
from .utils import is_admin, OPTS_CMD, MSG_JOBS

@keyword("admin")
@is_admin
@keyword.autohelp
def agregar_admin(irc, msg,trigger, args,kwargs):
    nick_ = args[0]
    nick = Access.objects.get(nick=nick_)
    nick.is_admin = not nick.is_admin
    nick.save()
    if nick.is_admin:
        msg.reply("%s es admin  " % nick.nick)
    else:
        msg.reply("%s ya no es admin" % nick.nick)

@keyword("aast")
@is_admin
@keyword.autohelp
def add_anime(irc, msg, trigger, args, kwargs):
    """<slug> <nombre de anime> :: agregar anime al showtimes"""
    slug = args[0]
    anime_name = " ".join(args[1:])
    
    anime_exist = Serie.objects.filter(slug__icontains=slug).exists()
    if anime_exist:
        msg.reply("dude ya existe un anime con ese dato")
        return

    s = Serie(slug=slug, name=anime_name)
    s.save()
    msg.reply("Creado anime %s - %s" % (s.slug, s.name))


@keyword("ast")
@is_admin
@keyword.autohelp
def add_showtime(irc, msg, trigger, args, kwargs):
    """<fansub_slug> <anime_slug>  <EpisodiosCompletados>/<TotaldeEpisodio> :: Agregar al showtime """

    anime_slug = args[1]
    fansub_slug = args[0]
    ec, et = args[2].split("/")

    exists = Work.objects.filter(anime__slug__icontains=anime_slug, fansub__slug__icontains=fansub_slug).exists()
    if exists:
        msg.reply("Alguien mas ya agrego ese anime al showtimes")
        return
    anime = Serie.objects.filter(slug__icontains=anime_slug)
    if not anime.exists():
        msg.reply("No existe anime con ese slug")
        return
    fansub = Fansub.objects.filter(slug__icontains=fansub_slug)
    if not fansub.exists():
        msg.reply("Fansub no existe")
    anime = anime[0]
    fansub = fansub[0]
    w = Work(anime=anime, fansub=fansub, chapters_completed=ec, chapters_total=et)
    w.save()
    msg.reply("Showtime agregado, ahora asigna tareas")

@keyword("asa")
@is_admin
def set_task(irc, msg, trigger, args, kwargs):
    doc = """!asa <fansub_slug> <anime_slug> <%s> <nick> :: asignar staff""" % "|".join([y for x,y in OPTS_CMD])
    if "help" in args[0]:
        msg.reply(doc)
        return 

    anime_slug = args[1]
    fansub_slug = args[0]
    task, nick = args[2], " ".join(args[3:])

    anime = Serie.objects.filter(slug__icontains=anime_slug)
    if not anime.exists():
        msg.reply("No existe anime con ese slug")
        return
    fansub = Fansub.objects.filter(slug__icontains=fansub_slug)
    if not fansub.exists():
        msg.reply("Fansub no existe")
    anime = anime[0]
    fansub = fansub[0]

    work = Work.objects.filter(anime__slug__icontains=anime_slug, fansub__slug__icontains=fansub_slug)[0]
    for job_key, alias_job  in OPTS_CMD:
        if task in alias_job:
            setattr(work, "%s_nick" % job_key, nick)
            work.save()
            tarea = dict(MSG_JOBS).get(job_key)
            msg.reply("Actualizando %s = %s" % (tarea, nick))
            return  
