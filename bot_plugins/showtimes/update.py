# encoding=utf8
from pyaib.plugins import keyword
from .utils import check_access, _searchserie, announce
import datetime

FORMAT = "%d-%m-%Y@%H:%M"

@keyword("act", "actualizar", "update")
def update(irc_c, msg, trigger, args, kwargs):
    t = check_access(msg.nick, msg)
    if not t:
        return
    def help():
        msg.reply("!act <fansub> <anime> XX/YY [DD-MM-YYYY@HH:MM]")

    if len(args) < 2 or "ayuda" in kwargs:
        help()
        return

    series = _searchserie("act", msg, args, kwargs)
    if not series:
        msg.reply("No se encontro el anime")
        return
    update = args[2]
    fecha = None
    if len(args) == 4:
        fecha = args[3]

    if len(args) ==5:
        to_all_channels = True
    else:
        to_all_channels = False

    finished, total = update.split("/")
    if fecha is not None:
        fecha = datetime.datetime.strptime(fecha,FORMAT)
        series.update(chapters_completed = int(finished), chapters_total = int(total), modified_at = fecha)
        l = []
        for i in series:
            l.append(i.anime.name)
        message = u"Se actualizó a %s a las %s horas de %s" % (",".join(l), fecha.strftime("%H:%M"), fecha.strftime("%d-%m-%Y"))
        announce(irc_c, msg, message, i.fansub.slug, to_all_channels)
        return

    for i in series:
        i.chapters_completed = int(finished)
        i.chapters_total = int(total)
        i.save()
        message = u"Se actualizó el anime: %s del fansub %s" %(i.anime.name, i.fansub.name)
        announce(irc_c, msg, message, i.fansub.slug, to_all_channels)

