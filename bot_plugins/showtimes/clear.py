# encoding=utf8
from pyaib.plugins import keyword
from airing.models import  Work as Season
from .utils import check_access
from .utils import OPTS_CMD, OPTS_ORDEN, OPTS, MSG_JOBS, MSG_OPTS

@keyword("clear", "listo")
@keyword.autohelp_noargs
def clear(irc_c, msg, trigger, args, kwargs):
    """!listo <fansub> <serie> [task]: pone todas las tareas en rojo"""
    t = check_access(msg.nick, msg)
    if not t:
        return

    def help():
        txt = "!listo <fansub> <serie> [task]: pone todas las tareas en rojo"
        msg.reply(txt)
    if 'ayuda' in kwargs or 'h' in kwargs:
        help()
        return
    act = "BNone"
    if len(args) == 2:
        fansub, serie= args
    elif len(args) == 3:
        fansub, serie, act = args
    else:
        help()
        return 

    series = Season.objects.filter(fansub__slug__icontains=fansub, anime__slug__icontains=serie)
    if not series:
        msg.reply("Anime no encontrados")
        return

    cmd_found = None
    for cmd, opts in OPTS_CMD:
        if act in opts:
            cmd_found = cmd
            break
    d = {}
    if not cmd_found:
        for i in OPTS:
            d.update({'%s_finished' % i:False})
    else:
        d = {'%s_finished' % cmd_found: False}

    series.update(**d)
    for i in series:
        if not cmd_found:
            msg.reply("Se ha actualizado el anime %s por parte de %s, ¡todos a vagacionar!" %
                  (i.anime.name, i.fansub.name))
        else:
            act = dict(MSG_JOBS).get(cmd_found)
            msg.reply("Marcando como no finalizado, %s por parte de %s, la tarea %s." %
                  (i.anime.name, i.fansub.name, act))

