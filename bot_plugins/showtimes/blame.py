# encoding=utf8
from pyaib.plugins import keyword
from airing.models import Work as Season
from django.utils.timesince import timesince
from .utils import  OPTS_ORDEN, OPTS, MSG_OPTS
from ..utils import colors

@keyword("vagos")
@keyword("haciendo")
def get_vagos(irc_c, msg, trigger, args, kwargs):
    def help():
        msg.reply("!vagos <fansub> <anime>")

    if len(args) < 2:
        help()
        return 
    for i in args:
        if "ayuda" in i:
            help(msg)
            return 
    txt = []
    fansub_slug, anime_slug = args[0], args[1]
    seasons = Season.objects.filter(anime__slug__icontains=anime_slug, fansub__slug__icontains = fansub_slug)
    for season in seasons:
        anime = season.anime.name
        txt_init = u"Los vagos que trabajan %s son:" % anime
        sorted_opts = sorted(dict(OPTS_ORDEN).items(), key=lambda x: x[1])
        for job,v in sorted_opts:
            attr_nick = getattr(season, "%s_nick" % job)
            job_ = dict(MSG_OPTS).get(job)
            txt.append( " %(job)s: %(nick)s" % dict(job=colors.color(job_, colors.BLUE), nick=colors.color(attr_nick, colors.RED)))
    if txt:
        txt = ",".join(txt)
        msg.reply(txt_init + txt)
    return 



@keyword("blame")
@keyword("vago")
@keyword.autohelp_noargs
def get_vago(irc_c, msg, trigger, args, kwargs):
    """<fansub> <anime> o !vago <nick>"""

    txt_init ="%s no has hecho (%s) tareas: "
    txt = []
    count = 0
    nick = msg.nick
    if len(args) == 0 or len(args)==1:
        if len(args)==1:
            nick = args[0]

        seasons = Season.objects.all()
        def check_0(nick, attr_nick, attr_job):
            if nick in attr_nick and attr_job == False:
                return True
            return False
        check = check_0

        for season in seasons:
            anime = season.anime.name
            for job in OPTS:
                attr_nick = getattr(season, "%s_nick" % job)
                attr_job = getattr(season, "%s_finished" % job)
                if attr_nick is None:
                    msg.reply("No se ah puesto ningun nick")
                    return 
                if check(nick, attr_nick, attr_job):
                    job_ = dict(MSG_OPTS).get(job)
                    count +=1
                    txt.append( " %(job)s del anime %(anime)s, episodio %(episo)s" % dict(job=job_, anime=anime, episo=season.chapters_completed))
        if txt:
            txt_init = txt_init % (nick, count)
            txt = ",".join(txt)
            msg.reply(txt_init + txt)
        else:
            msg.reply(" %s no tiene nada pendiente." % nick)
        return 

    if len(args) == 2:
        anime_slug = args[1]
        fansub_slug = args[0]
        seasons = Season.objects.filter(anime__slug__icontains=anime_slug, fansub__slug__icontains = fansub_slug)
        if not seasons.exists():
            msg.reply("No encontramos el anime y el fansub..")
        for season in seasons:
            anime_name = season.anime.name
            fansub_name = season.fansub.name
            txt_init = "[%s] %s, episodio %s, " % (colors.color(fansub_name, colors.BLUE), anime_name, str(season.chapters_completed).zfill(2) )
            txt = ""
            sorted_opts = sorted(dict(OPTS_ORDEN).items(), key=lambda x: x[1])
            all_done = True
            for job,v in sorted_opts:
                attr_nick = getattr(season, "%s_nick" % job)
                attr_job = getattr(season, "%s_finished" % job)
                job_ = dict(MSG_OPTS).get(job)
                if attr_job == False:
                    all_done = False
                    txt += "está atrasado por %(nick)s (%(job)s), " % dict(job=colors.color(job_, colors.RED), nick = colors.color(attr_nick, colors.NAVY_BLUE))
                    break
            if all_done:
                txt_init  += colors.color("¡Todo esta listo!. Falta publicarlo.", colors.PURPLE, colors.WHITE)
            else:
                txt += "desde hace %s." % ", ".join(timesince(season.modified_at).split(","))
            msg.reply(txt_init + txt)

