# encoding=utf8
from pyaib.plugins import keyword
from subbers.models import Access
from .utils import check_access


@keyword("agregar_nick", "add_nick")
@keyword.autohelp
def add_nick(irc_c, msg, trigger, args, kwargs):
    """ agregar nick """
    def help():
        msg.reply("!agregar_nick <nick>")
    if 'ayuda' in kwargs:
        help()
        return

    t = check_access(msg.nick, msg)
    if not t:
        return
    nick = args.pop()
    obj, created = Access.objects.get_or_create(nick=nick)
    if created:
        msg.reply("Agregado %s ahora showtime!" % obj.nick)
    else:
        msg.reply("Respondo a tus ordenes: %s" % obj.nick) 
 
