# encoding=utf8
from pyaib.plugins import keyword
from airing.models import Serie
from subbers.models import Fansub
from .fabfile_webm import main

URL = "http://zorabot.waifu.ca/showtimes"


@keyword("showtimes")
@keyword("show")
@keyword("st")
def show(irc_c, msg, trigger, args, kwargs):
    if len(args) == 0:
        msg.reply(URL)
        return
    anime_slug = None
    fansub_slug = None
    if len(args) >= 1:
        f = Fansub.objects.filter(slug__icontains=args[0])
        if not f.exists():
            msg.reply("No encontre fansub")
            return
        fansub = f[0]
        fansub_slug = f[0].slug
        anime = None
        if len(args) == 2:
            anime_slug = args[1]
            anime = Serie.objects.filter(slug__icontains=anime_slug)[0]
            anime_slug = anime.slug

    if fansub.showtime_url:
        msg.reply(fansub.showtime_url)
        return
    if fansub_slug and not anime_slug:
        msg.reply(URL+"?fansub=%s" % fansub_slug)
        return
    if fansub_slug and anime_slug:
        msg.reply(URL+"?fansub=%s#%s" % (fansub_slug, anime_slug))
        return
    msg.reply(URL)


@keyword("ayuda")
def ayuda(irc_c, msg, *args, **kwargs):
    msg.reply(u"Pide asistencia a algún miembro del staff de %s | ¿Tienes algún reporte de errores? Reportarlos aquí --> http://tinyurl.com/zorabug <-- | Para más información visítanos en http://zorabot.waifu.ca/showtimes/  " % msg.channel)

#@keyword("webm")
#@keyword.autohelp_noargs
def webm(irc_c, msg, trigger, args, kwargs):
    """ <crc32> <desde> <hasta> """
    str_srch, f, t = args
    msg.reply("generando webm espere un momento, echate un !chiste o una !bomba mientras termino ..")
    result = main(str_srch, f, t)
    msg.reply(" %s %s" % (msg.nick, result.values().pop()))
