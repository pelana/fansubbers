from pyaib.plugins import keyword
from .utils import check_access
import envoy 

@keyword("reiniciar")
def reiniciar(irc_c, msg,*args,**kwargs):
    t = check_access(msg.nick, msg)
    if not t:
        return 

    msg.reply("orbuaaa")
    irc_c.client.die()

@keyword("reiniciar_ch")
def reiniciar_ch(irc, msg, *args, **kwargs):
    t = check_access(msg.nick, msg)
    if not t:
        return 
    r = envoy.run("supervisorctl restart zorabot:chrd")
