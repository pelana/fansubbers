from fabric.api import *
from fabric.tasks import execute
from django.conf import settings
h = "xdcc@shinsoufansub.net"
PATH_DIR = "/home/bot/XDCC"
TMP_DIR = "/home/bot/tmp"
env.hosts = [h,]
#env.key_filename = "~/.ssh/id_rsa"
env.password=settings.SSH_PASSWORD

@task
def convert_webm(str_fnd, from_time=1, to_time=90):

    with cd(PATH_DIR):
        out = run("find . -iname '*%s*mkv'" % str_fnd)
        files = out.split("\r\n")
        assert len(files)!=0, "Demasiados archivos"
        mkv_file = files.pop()
        mkfile = mkv_file.split("/")[-1].replace(".mkv","")
        run("mkdir -p %s" % TMP_DIR)
        f = TMP_DIR+"/pre.mkv"
        run("cp %s %s" % (mkv_file, f))
        out_file_webm =TMP_DIR+"/"+mkfile+".webm"
        cmd = [
                "ffmpeg",
              #  " -loglevel panic",
                "-y -i '%s'" % f,
                "-vf 'subtitles=%s'" % f,
                "  -ss %s -t %s -threads 3" % (from_time, to_time),
                "-c:v libvpx  -c:a libvorbis",
                "-qmin 0 -qmax 50 -crf 5 -b:v 1M",
                "%s" % (out_file_webm,)
        ]
        run(" ".join(cmd))
        out = run('curl -s "https://puush.me/api/up" -# -F "k=550816F351C80B926772C2C37156B746" -F "z=poop" -F "f=@%s" | sed -E "s/^.+,(.+),.+,.+$/\\1\\n/"' % out_file_webm)
        run("rm %s" % f)
        run("rm %s" % out_file_webm)
        return out

def main(search_str, f, t):
    return execute(convert_webm, search_str, from_time=f, to_time=t)


if __name__ == "__main__":
    import sys
    search_str, f,t = sys.argv[1:]
    print main(search_str, f, t)
