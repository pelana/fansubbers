# encoding=utf8
from pyaib.plugins import keyword
from airing.models import Work as Season
from .utils import check_access, announce
from .utils import OPTS_CMD, OPTS_ORDEN, OPTS, MSG_JOBS, MSG_OPTS

@keyword("fin","finalizar", "done")
def done(irc, msg, trigger, args, kwargs):
    t = check_access(msg.nick, msg)
    if not t:
        return

    def help():
        cmds = "|".join(dict(OPTS_CMD).values())
        txt = "!fin <fansub> <serie> <%s>" % cmds
        msg.reply(txt)
    if 'ayuda' in kwargs or 'h' in kwargs:
        help()
        return

    if len(args) < 3:
        help()
        return
    if len(args) ==4:
        to_all_channels = True
    else:
        to_all_channels = False

    fansub, serie, act = args[0], args[1], args[2]
    series = Season.objects.filter(fansub__slug__icontains=fansub, anime__slug__icontains=serie)
    if not series:
        msg.reply("No encontre la serie %s  y el fansub %s, esta en la web? " % (
            serie, fansub))
        return 
    cmd_found = ""
    for cmd, opts in OPTS_CMD:
        if act in opts:
            cmd_found =cmd
            break
    if not cmd_found:
        msg.reply("No se encontró comando")
        return 
    for i in series:
        setattr(i, "%s_finished" % cmd_found, True)
        nick  = getattr(i, "%s_nick" % cmd_found)
        i.save()
        act_msg = dict(MSG_JOBS).get(cmd_found)
        txt_init = ( "Actualizando el anime %(anime)s episodio %(epi)s de %(fansub)s. %(nick)s, %(puesto)s, "
                "ha finalizado su trabajo. ")
        epi = "%s" % i.chapters_completed
        txt_init = txt_init % dict(anime=i.anime.name, puesto=act_msg, nick=nick, fansub=i.fansub.name, epi=epi.zfill(2))
        sorted_opts = sorted(dict(OPTS_ORDEN).items(), key=lambda x: x[1])
        all_done = True
        txt = ""
        for a,v in sorted_opts:
            attr = getattr(i, "%s_finished" % a)
            nick = getattr(i,"%s_nick" % a)
            act = dict(MSG_OPTS).get(a)
            if not attr:
                all_done = False
                txt += " %s termine %s." % (nick, act)
                break
        if not all_done:
            txt_init += "Lo siguiente es que"
        else:
            txt_init += " ¡Todo está listo!"
        if i.is_completed():
            txt +=u" >>TERMINADA<<"
        announce(irc, msg, txt_init + txt, i.fansub.slug, to_all_channels)

