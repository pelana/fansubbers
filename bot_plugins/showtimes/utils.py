# encoding=utf8
from subbers.models import Access, Fansub
from airing.models import Work as Season
import pprint
from pyaib.util.decorator import EasyDecorator


class is_added(EasyDecorator):
    def wrapper(dec, irc_c, msg, trigger,args, kwargs):
        access_exist =Access.objects.filter(nick=msg.nick).exist()
        if not access_exist:
            msg.reply("No tienes permisos de admin")
            return 

def is_admin_method(func):
    def wrapper( *args):
        _, irc_c, msg, trigger, args_, kwargs_= args
        access =Access.objects.get(nick=msg.nick)
        if not access.is_admin:
            msg.reply("No tienes permisos de admin")
            return 
        else:
            print args[3]
            return func(*args)
    return wrapper



def is_admin(func):
    def wrapper( irc_c, msg, trigger, args, kwargs):
        access =Access.objects.get(nick=msg.nick)
        if not access.is_admin:
            msg.reply("No tienes permisos de admin")
            return 
        else:
            return func(irc_c,msg, trigger,args, kwargs)
    return wrapper


def check_access(nick, msg):
    exists = Access.objects.filter(nick=nick).exists()
    if not exists:
        msg.reply("no puedo recibir ordenes de %s" % nick)
        return False
    return True

def _searchserie(cmd, msg, args, kwargs, leng=3):
    fansub, serie = args[0], args[1]
    series = Season.objects.filter(fansub__slug__icontains=fansub, anime__slug__icontains=serie)
    if not series:
        msg.reply("No encontre la serie %s  y el fansub %s, esta en la web? " % (
            serie, fansub))
        return 
    return series

def announce(context, msg, message,fansub_slug, to_all_channels=False):
    if not to_all_channels:
        msg.reply(message)
        return 

    fansub = Fansub.objects.get(slug= fansub_slug)
    ann = fansub.announce
    if not ann:
        msg.reply(message)
    channels = []
    for chan in ann.split(","):
        chan = chan.replace("#", "").lower()
        channels.append("#%s" % chan)
    if not  msg.channel in context.channels:
        channels.append(msg.channel)
    for chan in channels:
        if chan in context.channels:
            print (chan, message)
            context.PRIVMSG(chan, message)

OPTS = (
        "translator",
        "editor", 
        "typesetter",
        "timer", 
        "encoder",
        "quality_check1", 
        "quality_check2",
        "karaoke"
)

OPTS_ORDEN = (
    (OPTS[0],1),
    (OPTS[1],3),
    (OPTS[2],5),
    (OPTS[3],2),
    (OPTS[4],0),
    (OPTS[5],6),
    (OPTS[6],7),
    (OPTS[7],4),
)


MSG_OPTS= (
    (OPTS[0], u"Traducción",),
    (OPTS[1],u"Corrección"),
    (OPTS[2],u"Edición"),
    (OPTS[3],u"Sincronización"),
    (OPTS[4],u"Compresión"),
    (OPTS[5],"QC1"),
    (OPTS[6],"QC2"),
    (OPTS[7],"Karaoke"),
)

MSG_JOBS = (
    (OPTS[0], "Traductor",),
    (OPTS[1],"Corrector"),
    (OPTS[2],"Editor"),
    (OPTS[3],"Sicronizador"),
    (OPTS[4],u"Compresión"),
    (OPTS[5],"QC1"),
    (OPTS[6],"QC2"),
    (OPTS[7],"Karaoke"),
)
OPTS_CMD = (
    (OPTS[0], "tradu"),
    (OPTS[1],"crt"),
    (OPTS[2],"edit"),
    (OPTS[3],"src"),
    (OPTS[4],u"cpn"),
    (OPTS[5],"qc1"),
    (OPTS[6],"qc2"),
    (OPTS[7],"kara"),
)

