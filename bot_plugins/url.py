from pyaib.plugins import keyword, plugin_class, observe
from webscraping import download, xpath
import re 


@plugin_class
class FetchUrl(object):

    def __init__(self, irc_context, config):
        pass

    @observe("IRC_MSG")
    def irc_obsr(self, irc_c, msg):
        if "notice" in msg.raw.lower() or "zorabot" in msg.raw.lower():
            return 
        if "#hoshizora-staff" in msg.raw.lower():
            channel = "#hoshizora-staff"
        elif "#hoshizora" in msg.raw.lower():
            channel = "#hoshizora"
        elif "#botbot" in msg.raw:
            channel = "#botbot"
        else:
            channel = False

        if channel is not False:
            urls = self.get_url(msg.raw)
            for url in urls:
                title = self.get_title(url)
                title_ = self.format(title)
                irc_c.PRIVMSG(channel, title_)

    def format(self, title):
        return title

    def get_url(self, message):
        urls = re.findall(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', message)

        return urls

    def get_title(self, url):
        d = download.Download()        
        html = d.get(url)
        title = xpath.get(html, "//title")
        print title
        return title


if __name__ == "__main__":
    pr = FetchUrl(None, 1)
    pr.get_title("http://google.com")
