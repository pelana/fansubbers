#!/usr/bin/python
import ch
import redis


class TestBot(ch.RoomManager):
  key ="chatango"
  def onConnect(self, room):
    self.redis = redis.StrictRedis(host="localhost", port=6379)
    #self.redis.rpush(self.key, "Connected to "+room.name)
    print "connect"
  def onEventCalled(self, room, evt, *args, **kw):
    print room, evt, args, kw

  def onReconnect(self, room):
    self.redis.rpush(self.key,"Reconnected to "+room.name)

  def onDisconnect(self, room):
    self.redis.rpush(self.key, "Disconnected from "+room.name)
    import sys
    sys.exit(1)

  def onMessage(self, room, user, message):
    # Use with PsyfrBot framework? :3
    msg = user.name + ': ' + message.body
    print msg
    self.redis.rpush(self.key, msg) 

  def onFloodBan(self, room):
    msg = ("You are flood banned in "+room.name)
    self.redis.rpush(self.key, msg)    

  def onPMMessage(self, pm, user, body):
    msg = 'PM: ' + user.name + ': ' + body
    self.redis.rpush(self.key, msg)    

if __name__ == "__main__":
  TestBot.easy_start(rooms=["hoshizorasubs"], name="ircbot", password="")
