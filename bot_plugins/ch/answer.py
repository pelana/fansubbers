#!/usr/bin/python
import ch
import sys
msg = ""
import cgi

class TestBot(ch.RoomManager):

  def onConnect(self, room): 
      global msg
      #print "<span style='text-style:bold;'>send:</span> %s" % msg
      room.ping()
      nick, txt = msg.split(":")
      msg_ = u":".join([nick, txt.decode("utf-8").encode('ascii', 'xmlcharrefreplace')])
      print msg_
      room.message(msg_, html = True)
      room.ping()

if __name__ == "__main__":
    msg = " ".join(sys.argv[1:])
    TestBot.easy_start(rooms=["hoshizorasubs"], name="desdeirc", password="zxczxc")
