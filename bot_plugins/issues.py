from pyaib.plugins import keyword
from .showtimes.utils import is_admin

@keyword("bug", "bugs")
@keyword.autohelp_noargs
def bug(irc, msg, trigger, args, kwargs):
    """http://tinyurl.com/zorabug"""
    status = args[0]
    msg.reply("https://bitbucket.org/pelana/fansubbers/issues?&status=%s o ejececuta !bb" % status)


@keyword("entraen", "entraren", "join")
@is_admin
def entraen(irc, msg,trigger, args, kwargs):
    channel = args.pop()
    irc.JOIN(channel)

