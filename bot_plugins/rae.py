from pyaib.plugins import keyword, plugin_class
import requests
import speedparser
import html2text

@plugin_class
class Rae(object):

    def __init__(self, irc_context, config):
        self.base =  "http://dirae.es/palabras/?q=%s"
        self.url =self.base+"&output=atom"
    def fetch_rae(self, word):
        r = requests.get(self.url % word)
        cont = r.content
        result = speedparser.parse(cont)
        len_entry = result["entries"]
        res = []
        for e in len_entry:
            title = e["title"]
            href = e["link"]
            summary = html2text.html2text(e["summary"])
            res.append((title, href, summary))

        return res
    def format(self, msg):
        title, href, summary = msg
        txt = "[%s]: %s   " % (title, summary)
        return txt

    @keyword("rae")
    @keyword.autohelp
    def rae_fetc(self, irc, msg, trigger, args, kwargs):
        """ <words> """
        words = " ".join(args)
        res = self.fetch_rae(words)
        for r in res[0:4]:
            txt = self.format(r)
            msg.reply(txt)
        if len(res) > 4:
            msg.reply("mas resultados en %s" % (self.base %words,))


if __name__ == "__main__":
    pr = Rae(None, 1)
    pr.fetch_rae("cantinflear")
