from .showtimes import show, ayuda, webm
from .showtimes.add_nick import add_nick
from .showtimes.blame import get_vago, get_vagos
from .showtimes.done import done
from .showtimes.restart import reiniciar, reiniciar_ch
from .showtimes.update import update
from .showtimes.clear import clear
from .showtimes.add import add_anime, add_showtime, set_task, agregar_admin
from pyaib.plugins import keyword
from .showtimes.utils import is_admin


@keyword("entraen", "entraren", "join")
@is_admin
def entraen(irc, msg,trigger, args, kwargs):
    channel = args.pop()
    irc.JOIN(channel)

@keyword("doc","pad")
@keyword.channel("#hoshizora-staff")
@keyword.autohelp_noargs
def entraen(irc, msg,trigger, args, kwargs):
    """http://hoshizora.publishwith.me/ep/padlist/all-pads"""
    word = args.pop()
    msg.reply("http://hoshizora.publishwith.me/%s" % word)
