from pyaib.plugins import keyword, plugin_class
from bitbucket.bitbucket import Bitbucket
from django.conf import settings

@plugin_class
class BitbucketIRC(object):
    slug = "fansubbers"

    def __init__(self, irc_context, config):
        self.bb = Bitbucket(settings.BB_USER, settings.BB_PASSWORD, repo_name_or_slug =self.slug)

    def fetch_issues(self, status="new", cantidad = 3):
        bb = self.bb
        success, resp = bb.issue.all(params={'status': status})
        result = []
        for issue in resp["issues"][0:int(cantidad)]:
            id = issue["local_id"]
            str_i = " %s [%s] " %( issue["priority"], issue["title"])
            str_i +="reportado: %s " %(issue["reported_by"]["username"])
            url = "https://bitbucket.org/%s/%s/issue/%s" %(settings.BB_USER, self.slug,id)
            str_i += url
            result.append(str_i)
        return result
    
    @keyword("bb")
    @keyword.autohelp
    def issues(self, irc, msg, trigger, args, kwargs):
        """ ver los issues del bot """
        if len(args) == 0:
            result = self.fetch_issues()
        elif len(args) == 1:
            status = args[0]
            result = self.fetch_issues(cantidad=status)
        else:
            status = args[1]
            cant = args[0]
            result = self.fetch_issues(status, cant)
        #msg.reply(result)
        for i in result:
            irc.RAW("NOTICE %s :%s" % (msg.nick, i))


if __name__ == "__main__":
    bb = BitbucketIRC()
    print bb.fetch_issues()
