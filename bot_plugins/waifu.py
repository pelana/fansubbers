from pyaib.plugins import  plugin_class, every, keyword
import os
import os.path
import requests

import envoy 

@plugin_class("waifu")
class Waifu(object):
    def __init__(self, context, config):
       self.slug = ""

    @keyword("run")
    @keyword.private
    def run(self, irc_c, msg, trigger, args, kwargs):
        msg.reply("fetching .... wait")
        envoy.run("bash /home/zodman/apps/supranime_torrent/run.bash")
        msg.reply("terminado!")


    @keyword("last")
    def last_chapter(self, irc_c, msg, trigger, args, kwargs):
        res = requests.get("http://anime.waifu.ca/s/today").json()
        msg_ = "Ultimo capitulo de waifu es %(title)s %(chapter)sx%(season)s creado: %(fecha)s" % res
        msg.reply(msg_)

    @every(seconds=5, name="feed2")
    def check_new(self, *args, **kwargs):
        a = args[0]
        Irc_c = a.get("client").irc_c
        res = requests.get("http://anime.waifu.ca/s/today").json()
        if res["slug"] != self.slug:
            self.slug = res["slug"]
            data = {'value1':res.get("title"),'value2': "%s" % res.get("chapter")}
            requests.post("https://maker.ifttt.com/trigger/update/with/key/br8s-GkzPAnWiQDC_jwuqT", data=data)
            msg ="Se agrego a waifu %(title)s %(chapter)sx%(season)s" % res
            Irc_c.PRIVMSG("#waifuapp", msg)


   
