import gdshortener

def shorten(url):
    try:
        s = gdshortener.ISGDShortener()
        url, stat  = s.shorten(url)
    except gdshortener.GDBaseException:
        url = url
    return "%s" % url
