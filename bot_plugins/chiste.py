# encoding=utf8
from pyaib.plugins import observe, keyword, plugin_class, every
from webscraping import download, xpath
import requests
import random
import time
import HTMLParser


@plugin_class("chiste")
class Chiste(object):

    def __init__(self, irc_context, config):
        print("loaded chiste plugin")
        self.html_parser = HTMLParser.HTMLParser()

    def scrape_joke(self):
        self.D = download.Download()
        d = dict(read_cache=False, write_cache=False, cache= False)
        html = self.D.get("http://www.chistes.com/ChisteAlAzar.asp?n=3", **d)
        r= xpath.get(html,'//div[@class="chiste"]/text()')
        return r.decode("latin").split("\n")

    def scrape_bomba(self):
        self.D2 = download.Download()
        html = self.D2.get("http://www.moonyoz.com/bombasyucatecas/bombas_picarezcas.asp")
        r= xpath.search(html,'//b/font/p/text()')
        r = random.choice(r)
        return r.decode("latin").split("\n")
    
    @keyword("bomba")
    @keyword.autohelp
    def said_a_bomba(self,irc_c, msg, trigger, args, kargs):
        """:: for getting a bomba yucateca """
        r = self.scrape_bomba()
        for i in r:
            msg.reply( "%s" % i)

    @keyword("chiste")
    @keyword.autohelp
    def said(self,irc_c, msg, trigger, args, kargs):
        """:: for getting a chiste from chiste.com """
        r = self.scrape_joke()
        for i in r:
            msg.reply( "%s" % i)

    @keyword("lol")
    @keyword.autohelp
    def reaction(self,irc, msg, trigger, args, kwargs):
        """ lol from r/reactiongifs"""
        d_args = dict(read_cache=False, write_cache=False, cache= False)
        d = download.Download()
        html = d.get("http://www.reddit.com/r/reactiongifs", **d_args)
        urls = xpath.search(html,"//a[@class='title may-blank']/@href")
        url = random.choice(urls)
        msg.reply(url)

