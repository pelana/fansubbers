from pyaib.plugins import every, keyword
from .showtimes.utils import check_access
import envoy
import redis
key = "chatango"
url = "http://ow.ly/FVuyG"
import os
import HTMLParser
from utils import colors

BASE_DIR = os.path.dirname(__file__)

@keyword("ch")
@keyword.autohelp
def show(irc_c, msg,t,args,kwargs):
    """ manda mensajes al chatango de hoshizora"""
    s = check_access(msg.nick, msg)
    if not s:
        return 
    msg_ = "<b>@%s</b>: %s" % ( msg.nick, " ".join(args))
    cmd = "timeout 5 python %s/ch/answer.py '%s'" % (BASE_DIR, msg_)
    r = envoy.run(cmd)
    if r.status_code !=0:
        print (r.std_out,)
        print (r.std_err,)
        return 
    irc_c.RAW("NOTICE %s :%s" %(msg.nick, "Mensaje enviado %s" %url))

@every(2)
def chatango(irc_context, timer_name):
    client = irc_context.get("client").irc_c
    red = redis.Redis(host="localhost", port=6379)
    while red.llen(key)>0:
        txt = red.lpop(key)
        h = HTMLParser.HTMLParser()
        txt_ = h.unescape(txt)
        txt1=  txt_
        msg_ = "%s - %s" % ( txt1 , url)
        client.PRIVMSG("#hoshizora-staff", msg_)
