# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0018_auto_20141021_1223'),
    ]

    operations = [
        migrations.AlterField(
            model_name='season',
            name='chapters_completed',
            field=models.IntegerField(default=0),
        ),
    ]
