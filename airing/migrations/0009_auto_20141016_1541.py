# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0008_serie_slug'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='season',
            name='chapters',
        ),
        migrations.AddField(
            model_name='season',
            name='chapters_completed',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='season',
            name='chapters_total',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
