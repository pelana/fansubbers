# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0029_auto_20150731_1516'),
    ]

    operations = [
        migrations.RenameField(
            model_name='work',
            old_name='revision',
            new_name='revision_nick',
        ),
        migrations.AddField(
            model_name='work',
            name='revision_finished',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
