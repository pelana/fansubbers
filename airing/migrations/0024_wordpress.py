# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0023_auto_20141112_1529'),
    ]

    operations = [
        migrations.CreateModel(
            name='Wordpress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField()),
                ('title', models.CharField(max_length=100)),
                ('body', models.TextField()),
                ('categorias', models.CharField(max_length=100)),
                ('tags', models.CharField(max_length=100)),
                ('serie', models.OneToOneField(to='airing.Serie')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
