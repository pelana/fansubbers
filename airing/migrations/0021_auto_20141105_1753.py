# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0020_remove_serie_trakt_tv'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='season',
            name='assembly_finished',
        ),
        migrations.RemoveField(
            model_name='season',
            name='assembly_nick',
        ),
        migrations.AlterField(
            model_name='season',
            name='chapters_completed',
            field=models.IntegerField(default=0, verbose_name=b'Episodios en proceso'),
        ),
        migrations.AlterField(
            model_name='season',
            name='editor_nick',
            field=models.CharField(help_text=b'Correcion', max_length=100),
        ),
        migrations.AlterField(
            model_name='season',
            name='typesetter_nick',
            field=models.CharField(help_text=b'Edicion', max_length=100),
        ),
    ]
