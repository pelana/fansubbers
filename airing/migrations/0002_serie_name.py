# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='serie',
            name='name',
            field=models.CharField(default='', help_text=b'only for referer', max_length=20),
            preserve_default=False,
        ),
    ]
