# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0024_wordpress'),
    ]

    operations = [
        migrations.AddField(
            model_name='work',
            name='url',
            field=models.URLField(default=''),
            preserve_default=False,
        ),
    ]
