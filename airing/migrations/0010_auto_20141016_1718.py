# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0009_auto_20141016_1541'),
    ]

    operations = [
        migrations.AddField(
            model_name='season',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 10, 16), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='season',
            name='modified_at',
            field=models.DateTimeField(default=datetime.date(2014, 10, 16), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='serie',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 10, 16), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='serie',
            name='modified_at',
            field=models.DateTimeField(default=datetime.date(2014, 10, 16), auto_now=True),
            preserve_default=False,
        ),
    ]
