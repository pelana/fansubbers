# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0025_work_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='work',
            name='status',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
