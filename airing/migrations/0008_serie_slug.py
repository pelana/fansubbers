# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0007_season_completed'),
    ]

    operations = [
        migrations.AddField(
            model_name='serie',
            name='slug',
            field=models.SlugField(default='slug1'),
            preserve_default=False,
        ),
    ]
