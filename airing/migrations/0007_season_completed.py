# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0006_auto_20141015_2027'),
    ]

    operations = [
        migrations.AddField(
            model_name='season',
            name='completed',
            field=models.PositiveIntegerField(default=0, choices=[(0, b'Por conteo de torrents'), (1, b'Completada')]),
            preserve_default=True,
        ),
    ]
