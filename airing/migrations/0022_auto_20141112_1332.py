# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0017_fansub_showtime_url'),
        ('airing', '0021_auto_20141105_1753'),
    ]

    operations = [
        migrations.CreateModel(
            name='Work',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_search', models.CharField(max_length=50)),
                ('title_search_exclude', models.CharField(max_length=50, null=True, blank=True)),
                ('chapters_completed', models.IntegerField(default=0, verbose_name=b'Episodios en proceso')),
                ('chapters_total', models.IntegerField(default=0)),
                ('editor_nick', models.CharField(help_text=b'Correcion', max_length=100)),
                ('editor_finished', models.BooleanField(default=False)),
                ('translator_nick', models.CharField(max_length=100)),
                ('translator_finished', models.BooleanField(default=False)),
                ('typesetter_nick', models.CharField(help_text=b'Edicion', max_length=100)),
                ('typesetter_finished', models.BooleanField(default=False)),
                ('timer_nick', models.CharField(max_length=100)),
                ('timer_finished', models.BooleanField(default=False)),
                ('encoder_nick', models.CharField(max_length=100)),
                ('encoder_finished', models.BooleanField(default=False)),
                ('quality_check1_nick', models.CharField(max_length=100)),
                ('quality_check1_finished', models.BooleanField(default=False)),
                ('quality_check2_nick', models.CharField(max_length=100)),
                ('quality_check2_finished', models.BooleanField(default=False)),
                ('karaoke_nick', models.CharField(max_length=100)),
                ('karaoke_finished', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('anime', models.ForeignKey(to='airing.Serie')),
                ('fansub', models.ForeignKey(to='subbers.Fansub')),
            ],
            options={
                'ordering': ('-modified_at',),
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='season',
            name='anime',
        ),
        migrations.DeleteModel(
            name='Season',
        ),
        migrations.RemoveField(
            model_name='serie',
            name='fansub',
        ),
    ]
