# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0022_auto_20141112_1332'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='work',
            name='title_search',
        ),
        migrations.RemoveField(
            model_name='work',
            name='title_search_exclude',
        ),
        migrations.AlterField(
            model_name='work',
            name='karaoke_nick',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='work',
            name='quality_check2_nick',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
    ]
