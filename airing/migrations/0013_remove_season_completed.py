# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0012_auto_20141016_1638'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='season',
            name='completed',
        ),
    ]
