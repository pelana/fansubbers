# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0015_auto_20141017_1340'),
    ]

    operations = [
        migrations.AddField(
            model_name='season',
            name='assembly_finished',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='season',
            name='assembly_nick',
            field=models.CharField(default='', max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='season',
            name='karaoke_finished',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='season',
            name='karaoke_nick',
            field=models.CharField(default='', max_length=10),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='serie',
            name='name',
            field=models.CharField(max_length=100),
        ),
    ]
