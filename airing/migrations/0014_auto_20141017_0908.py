# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0013_remove_season_completed'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='season',
            name='name',
        ),
        migrations.AddField(
            model_name='season',
            name='quality_check1_finished',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='season',
            name='quality_check1_nick',
            field=models.CharField(default='', max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='season',
            name='quality_check2_finished',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='season',
            name='quality_check2_nick',
            field=models.CharField(default='', max_length=10),
            preserve_default=False,
        ),
    ]
