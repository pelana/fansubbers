# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0026_work_status'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='work',
            options={'ordering': ('anime__name', '-modified_at')},
        ),
        migrations.AlterField(
            model_name='work',
            name='status',
            field=models.BooleanField(default=False, editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='work',
            name='url',
            field=models.URLField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
