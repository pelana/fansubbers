# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0019_auto_20141021_1224'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='serie',
            name='trakt_tv',
        ),
    ]
