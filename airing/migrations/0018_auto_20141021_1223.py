# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0017_auto_20141019_0143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='season',
            name='chapters_total',
            field=models.IntegerField(default=0),
        ),
    ]
