# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0011_auto_20141016_1608'),
    ]

    operations = [
        migrations.AddField(
            model_name='season',
            name='timer_finished',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='season',
            name='timer_nick',
            field=models.CharField(default='', max_length=10),
            preserve_default=False,
        ),
    ]
