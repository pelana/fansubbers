# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0007_auto_20141015_2014'),
        ('airing', '0004_auto_20141014_0020'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='chapter',
            name='season',
        ),
        migrations.RemoveField(
            model_name='chapterjob',
            name='chapter',
        ),
        migrations.DeleteModel(
            name='Chapter',
        ),
        migrations.RemoveField(
            model_name='chapterjob',
            name='job',
        ),
        migrations.DeleteModel(
            name='ChapterJob',
        ),
        migrations.RemoveField(
            model_name='job',
            name='fansub',
        ),
        migrations.DeleteModel(
            name='Fansub',
        ),
        migrations.DeleteModel(
            name='Job',
        ),
        migrations.RemoveField(
            model_name='serie',
            name='myanimelist_id',
        ),
        migrations.AddField(
            model_name='season',
            name='chapters',
            field=models.PositiveIntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='serie',
            name='fansub',
            field=models.ForeignKey(default=1, to='subbers.Fansub'),
            preserve_default=False,
        ),
    ]
