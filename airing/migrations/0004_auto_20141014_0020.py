# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0003_auto_20141013_2355'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChapterJob',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('percent', models.PositiveIntegerField()),
                ('chapter', models.ForeignKey(to='airing.Chapter')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('type', models.CharField(max_length=10, choices=[(b'T', b'Traduccion'), (b'E', b'Edicion'), (b'QC', b'Quality Check')])),
                ('who', models.CharField(help_text=b'nick', max_length=10)),
                ('fansub', models.ForeignKey(to='airing.Fansub')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='chapterjob',
            name='job',
            field=models.ForeignKey(to='airing.Job'),
            preserve_default=True,
        ),
        migrations.RemoveField(
            model_name='serie',
            name='fansubs',
        ),
    ]
