# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0016_auto_20141018_2310'),
    ]

    operations = [
        migrations.AlterField(
            model_name='season',
            name='assembly_nick',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='season',
            name='editor_nick',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='season',
            name='encoder_nick',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='season',
            name='karaoke_nick',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='season',
            name='quality_check1_nick',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='season',
            name='quality_check2_nick',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='season',
            name='timer_nick',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='season',
            name='translator_nick',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='season',
            name='typesetter_nick',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='serie',
            name='name',
            field=models.CharField(max_length=1000),
        ),
    ]
