# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airing', '0014_auto_20141017_0908'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='season',
            options={'ordering': ('-modified_at',)},
        ),
    ]
