from django.db import models
from subbers.models import Fansub

class Serie(models.Model):
    name = models.CharField(max_length=1000)
    slug = models.SlugField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s " % (self.name)


class Wordpress(models.Model):
    slug = models.SlugField()
    title = models.CharField(max_length=100)
    body = models.TextField()
    categorias = models.CharField(max_length=100)
    tags = models.CharField(max_length=100)
    serie = models.OneToOneField("Serie")

    def __unicode__(self):
        return u"%s" % self.title



class Work(models.Model):
    anime = models.ForeignKey(Serie)
    fansub = models.ForeignKey(Fansub)
    url = models.URLField(blank=True, null=True)
    chapters_completed = models.IntegerField(default=0, verbose_name="Episodios en proceso" )
    chapters_total = models.IntegerField(default=0)

    editor_nick = models.CharField(max_length=100, help_text="Correcion", )
    editor_finished = models.BooleanField(default=False)

    translator_nick = models.CharField(max_length=100)
    translator_finished = models.BooleanField(default=False)

    typesetter_nick = models.CharField(max_length=100,help_text="Edicion")
    typesetter_finished = models.BooleanField(default=False)

    timer_nick =  models.CharField(max_length=100)
    timer_finished = models.BooleanField(default=False)

    encoder_nick =  models.CharField(max_length=100)
    encoder_finished = models.BooleanField(default=False)

    quality_check1_nick =  models.CharField(max_length=100, )
    quality_check1_finished = models.BooleanField(default=False)
    quality_check2_nick = models.CharField(max_length=100, blank =True, null =True, )
    quality_check2_finished = models.BooleanField(default=False)


    karaokesync_nick =  models.CharField(max_length=100, blank =True, null =True, )
    karaokesync_finished = models.BooleanField(default=False)

    karaoke_nick =  models.CharField(max_length=100, blank =True, null =True, )
    karaoke_finished = models.BooleanField(default=False)
    status = models.BooleanField(default=False, editable = False)
    revision_nick = models.CharField(max_length=100, null =True, blank =True)
    revision_finished = models.BooleanField(default=False)
    greets = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True,editable = True)

    class Meta:
        ordering = ("anime__name","-modified_at",)

    def __unicode__(self):
        return "%s - %s " % (self.anime, self.pk)

    def save(self, *args, **kwargs):
        self.status = self.is_completed()
        super(Work,self).save(*args, **kwargs)

    def __finish_task(self):

        from bot_plugins.showtimes.utils import OPTS
        for i in OPTS:
            task_finish = getattr(self, "%s_finished" % i)
            if not task_finish:
                return False
        return True

    def is_completed(self):
        season = self
        if season.chapters_completed > 0  and \
           season.chapters_completed == season.chapters_total and \
           self.__finish_task():
            return True
        return False


