from django.views.generic import ListView, DetailView
from .models import Work as Season
from subbers.models import Fansub

class Status(ListView):
    model = Season
    template_name = "airing/iframe.html"

    def get_queryset(self):
        qs = super(Status, self).get_queryset()
        fansub = self.request.GET.get("fansub")
        if fansub:
            qs = qs.filter(fansub__slug__icontains=fansub)
        return qs

    def get_context_data(self,**kwargs):
        context = super(Status, self).get_context_data(**kwargs)
        fansub_id = self.request.GET.get("fansub")
        if fansub_id:
            fansub = Fansub.objects.get(slug=fansub_id)
            context.update({'fansub':fansub})
        return context 


status = Status.as_view()

class Iframe(Status):
    template_name = "airing/iframe.html"
iframe = Iframe.as_view()

detail = DetailView.as_view(model=Season)
