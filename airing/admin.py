from django.contrib import admin
from .models import Serie, Work, Wordpress


class WordpressInline(admin.TabularInline):
    model = Wordpress

class SerieAdmin(admin.ModelAdmin):
    list_display = ('name',)
    prepopulated_fields ={'slug': ("name",)}
    search_fields = ("name", "slug")
    inlines = [WordpressInline,]

class WorkAdmin(admin.ModelAdmin):
    list_display = ("fansub","anime","modified_at")
    raw_id_fields = ("anime","fansub")

admin.site.register(Serie, SerieAdmin)
admin.site.register(Work, WorkAdmin)
