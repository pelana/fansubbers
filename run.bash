source ~/.virtualenvs/fansubbers/bin/activate
cd ~/fansubbers
python manage.py populate
python manage.py update_index --noinput
