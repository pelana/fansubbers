#!/usr/bin/env python
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)


from pyaib.ircbot import IrcBot
from gevent.monkey import patch_subprocess
patch_subprocess()

import sys
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fansubs.settings")
import django
django.setup()
argv = sys.argv[1:]

#Load 'botbot.conf' from the par
bot = IrcBot(argv[0] if argv else 'botbot.conf')

print("Config Dump: %s" % bot.config)

#Bot Take over
bot.run()
