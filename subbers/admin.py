from django.contrib import admin
from .models import  Entry, Fansub, Regex, Access

class RegexIndex(admin.TabularInline):
    model = Regex
    extra = 1


class FansubAdmin(admin.ModelAdmin):
#    inlines = [RegexIndex, ]
    prepopulated_fields ={'slug': ("name",)}

class EntryAdmin(admin.ModelAdmin):
    list_display = ("title", "fansub","published_at")
    list_filter = ("fansub",)
    date_hierarchy = "published_at"
    search_fields = ("fansub__name", "title", "link")

class AccessAdmin(admin.ModelAdmin):
    search_fields = ("nick",)
    list_display = ("nick", "is_admin")


#admin.site.register(Entry, EntryAdmin)
admin.site.register(Fansub, FansubAdmin)
admin.site.register(Access, AccessAdmin)
