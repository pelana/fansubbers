from django.core.management.base import BaseCommand
from django.conf import settings 
from django.utils import timezone
from django.core.management import call_command
import logging
import pytz
import hashlib
import base64
import requests
import feedparser
from time import mktime
from subbers.models import Fansub, Entry
import urllib
import requests_cache
import bencode

requests_cache.install_cache('entry_cache')

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    def handle(self, *args, **options):
        if len(args) > 0:
            pager = int(args[0])
        else:
            pager = 1

        for f in Fansub.objects.all():
            print "populate %s" % f.name
            self.populate(f.nyaa_id, f, pager)

    def gen_magnet(self, torrent_url):
        print "gen %s" % torrent_url
        r = requests.get(torrent_url)
        metadata = bencode.bdecode(r.content)
        hashcontents = bencode.bencode(metadata['info'])
        digest = hashlib.sha1(hashcontents).digest()
        b32hash = base64.b32encode(digest)
        if "length" in metadata['info'].keys():
            xl = metadata['info']['length']
        else:
            xl=0 
        params = {'xt': 'urn:btih:%s' % b32hash,
                   'dn': metadata['info']['name'],
                  'tr': metadata['announce'],
                  'xl': xl}
        paramstr = urllib.urlencode(params)
        magneturi = 'magnet:?%s' % urllib.unquote(paramstr)
        return magneturi

    def populate(self, user_id, fansub, offset=1):

        URL ="http://www.nyaa.se/?page=rss&user=%s" % user_id
        feeds = []
        for i in range(1,offset+1):
            feed = URL + "&offset=%s" %i
            feeds.append(feed)

        for url_feed in feeds:
            print url_feed
            feed = feedparser.parse(url_feed)
            print "fetch"
            for i in feed["items"]:
                title = i["title"].replace("_"," ")
                id = i["id"]
                torrent_link = i["link"]
                published = timezone.datetime.fromtimestamp(mktime(i["published_parsed"]))
                summary = i["summary"]
                magnet = self.gen_magnet(torrent_link)
                d = dict(title=title, fansub=fansub, link=torrent_link)
                data = dict(link=torrent_link, published_at=published.replace(tzinfo=pytz.UTC),
                    title=title, summary=summary, magnet = magnet
                )
                d.update({"defaults":data})
                if not id:
                    return 
                try:
                    Entry.objects.update_or_create( **d)
                except Exception as e:
                    print (e, d.get("defaults"))
            print "next"

