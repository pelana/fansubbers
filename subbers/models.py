from django.db import models
import re

class Regex(models.Model):
    regex = models.TextField()
    fansub = models.ForeignKey("Fansub")
    order = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ("-order",)



class Fansub(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField()
    nyaa_id = models.CharField(max_length=100)
    websites = models.CharField(max_length=400,blank=True, null=True)
    showtime_url = models.URLField(blank=True, null=True)
    logo = models.URLField(blank=True, null=True)
    irc = models.CharField(blank=True, null=True, max_length=100)
    announce = models.CharField(max_length=200, blank=True, null=True, help_text="ejemplo: *,#chan1,chan2")
    description_subtitles = models.TextField()
    description_subtitles_alter = models.TextField(blank=True, null=True)
    description_nyaa = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)


    def __unicode__(self):
        return u"%s" %self.name

class Entry(models.Model):
    fansub = models.ForeignKey(Fansub)
    summary = models.TextField()
    title = models.TextField()
    link  = models.URLField()
    magnet = models.TextField(null=True,blank=True)
    published_at = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title

    class Meta:
        unique_together = ("fansub", "title" )
        ordering = ("-published_at",)

    def _parse(self):
        title = self.title.replace("_"," ").replace("HDTV","720p")
        for i in self.fansub.regex_set.all():
            regex_str = i.regex
            m = re.match(regex_str, title, re.UNICODE)
            result =None
            if m:
                result = m.groupdict()
                return result

    @property
    def data(self):
        return self._parse()
    @property
    def info(self):
        return self.link.replace("download","view")

class Access(models.Model):
    nick = models.CharField(max_length=10)
    is_admin = models.BooleanField(default=False)
    def __unicode__(self):
        return self.nick
