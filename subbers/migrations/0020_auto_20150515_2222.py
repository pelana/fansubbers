# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0019_fansub_description_nyaa'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fansub',
            name='website',
        ),
        migrations.AddField(
            model_name='fansub',
            name='description_subtitles',
            field=models.TextField(default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='fansub',
            name='websites',
            field=models.CharField(max_length=400, null=True, blank=True),
            preserve_default=True,
        ),
    ]
