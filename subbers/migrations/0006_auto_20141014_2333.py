# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0005_auto_20141014_2255'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='regex',
            options={'ordering': ('order',)},
        ),
        migrations.AddField(
            model_name='regex',
            name='order',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
