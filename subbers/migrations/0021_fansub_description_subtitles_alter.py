# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0020_auto_20150515_2222'),
    ]

    operations = [
        migrations.AddField(
            model_name='fansub',
            name='description_subtitles_alter',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
