# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0018_access_is_admin'),
    ]

    operations = [
        migrations.AddField(
            model_name='fansub',
            name='description_nyaa',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
