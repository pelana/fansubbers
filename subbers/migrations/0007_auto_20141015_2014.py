# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0006_auto_20141014_2333'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='regex',
            options={'ordering': ('-order',)},
        ),
    ]
