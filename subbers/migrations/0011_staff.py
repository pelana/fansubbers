# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0010_fansub_slug'),
    ]

    operations = [
        migrations.CreateModel(
            name='Staff',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nick', models.CharField(max_length=30)),
                ('grade', models.CharField(help_text=b'owner, colider, founder, chalan, etc', max_length=30)),
                ('activity', models.CharField(max_length=1, choices=[(b'0', b'Ninguno'), (b'1', b'Traductor'), (b'2', b'Editor'), (b'3', b'Timer'), (b'4', b'Corrector'), (b'5', b'encoder')])),
                ('fansub', models.ForeignKey(to='subbers.Fansub')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
