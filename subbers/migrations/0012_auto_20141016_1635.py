# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0011_staff'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='staff',
            name='fansub',
        ),
        migrations.DeleteModel(
            name='Staff',
        ),
    ]
