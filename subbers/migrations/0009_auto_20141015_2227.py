# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0008_entry_magnet'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='magnet',
            field=models.TextField(null=True, blank=True),
        ),
    ]
