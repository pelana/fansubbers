# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0004_auto_20141013_2324'),
    ]

    operations = [
        migrations.CreateModel(
            name='Regex',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('regex', models.TextField()),
                ('fansub', models.ForeignKey(to='subbers.Fansub')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='fansub',
            name='regex',
        ),
    ]
