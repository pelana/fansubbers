# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0009_auto_20141015_2227'),
    ]

    operations = [
        migrations.AddField(
            model_name='fansub',
            name='slug',
            field=models.SlugField(default='a'),
            preserve_default=False,
        ),
    ]
