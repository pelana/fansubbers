# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0007_auto_20141015_2014'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='magnet',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
