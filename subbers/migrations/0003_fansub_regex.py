# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0002_auto_20141009_2324'),
    ]

    operations = [
        migrations.AddField(
            model_name='fansub',
            name='regex',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
