# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0015_fansub_irc'),
    ]

    operations = [
        migrations.AddField(
            model_name='fansub',
            name='announce',
            field=models.CharField(help_text=b'ejemplo: *,#chan1,chan2', max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
    ]
