# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subbers', '0003_fansub_regex'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='serieentry',
            name='entry',
        ),
        migrations.RemoveField(
            model_name='serieentry',
            name='serie',
        ),
        migrations.DeleteModel(
            name='Serie',
        ),
        migrations.DeleteModel(
            name='SerieEntry',
        ),
    ]
