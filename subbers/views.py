from django.shortcuts import render
from django.views.generic import ListView, TemplateView, DetailView
from .models import Entry, Fansub


class Offline(TemplateView):
    template_name ="offline.html"

index_offline = Offline.as_view()

class IndexView(ListView):
    model = Entry

    def get_queryset(self):
        qs = super(IndexView, self).get_queryset()
        fansub_id = self.request.GET.get("fansub")
        if fansub_id:
            qs = qs.filter(fansub__slug__icontains=fansub_id)
        return qs

    def get_context_data(self,**kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        fansub_id = self.request.GET.get("fansub")
        if fansub_id:
            fansub = Fansub.objects.get(slug=fansub_id)
            context.update({'fansub':fansub})
        return context 

index = IndexView.as_view()


detail = DetailView.as_view(model=Entry)
