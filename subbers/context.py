from .models import Fansub

def fansubs(request):
    return {'fansubs': Fansub.objects.all()}
