from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from subbers.models import Fansub
from airing.models import Serie, Wordpress
from tastypie.fields import OneToOneField

class WordpressResource(ModelResource):
    class Meta:
        queryset = Wordpress.objects.all()
        filtering = {'slug':ALL, 'name':ALL}

class SerieResource(ModelResource):
    wordpress = OneToOneField(WordpressResource, "wordpress", related_name="series")
    class Meta:
        queryset = Serie.objects.all()
        filtering = {'slug':ALL, 'name':ALL}


class EntryResource(ModelResource):
    class Meta:
        queryset = Fansub.objects.all()
        filtering = {'slug':ALL, 'name':ALL}
