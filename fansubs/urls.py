from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView, RedirectView
from django.conf.urls.static import static 
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from subbers.views import index, index_offline
from airing.views import status, iframe, detail
import haystack
from tastypie.api import Api
from .api import EntryResource, SerieResource, WordpressResource
api = Api(api_name="v1")

api.register(EntryResource())
api.register(SerieResource())
api.register(WordpressResource())

urlpatterns = [
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^showtimes/$', status, name='status'),
    url(r'^api/', include(EntryResource().urls)),
    url(r'^api/', include(api.urls)),
    url(r'^iframe/$', iframe, name='iframe'),
#    url(r'^$', index, name='home'),
    url(r'^$', RedirectView.as_view(url='/showtimes/'), name='home'),
    url(r'^showtimes/(?P<pk>\d+)/$', detail, name='detail'),
    url(r'^admin/', include(admin.site.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + staticfiles_urlpatterns()
